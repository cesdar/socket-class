#ifndef CSOCKET_CLASS
#define CSOCKET_CLASS

#include "socketIncludes.h"
#include "CSocket.cpp"
#include <inttypes.h>
#include <string>

class CSocket
{
public:
    CSocket(int af, int type, int protocol);
    virtual ~CSocket();
    virtual void setHostname(std::string hostname);
    virtual void setHostname(char* hostname);
    virtual void init();
    virtual void bind(std::string hostname, uint16_t port);

private:
    static bool socketsInitialised;
    static uint16_t m_instaces;
    SOCKET m_socket;
    std::string m_hostname;
    uint16_t m_portnr;
    struct sockaddr_in m_sockaddr;
};




#endif