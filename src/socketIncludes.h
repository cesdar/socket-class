#ifndef SOCKET_INCLUDES
#define SOCKET_INCLUDES

#ifdef _WIN32
#include <windows.h>
#include <winsock.h>

#define CLOSE_SOCK(SOCK) closesocket(SOCK)
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#define SOCKET int
#define SOCKET_ERROR -1
#define CLOSE_SOCK(SOCK) close(SOCK)
#endif

#endif