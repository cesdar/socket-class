#include "CSocket.hpp"

bool CSocket::socketsInitialised = FALSE;
uint16_t CSocket::m_instaces = 0;

CSocket::CSocket(int af, int type, int protocol) : 
    m_socket(0)
{
    //First Initialisation of Sockets before first use
    if(m_instaces == 0)
    {
        //Initialisation for Windows Sockets
        #ifdef _WIN32
        WSADATA wsaData;
        WSAStartup(MAKEWORD(2,0),&wsaData);
        //Initialisation for all other Platforms
        #else

        #endif
        socketsInitialised = TRUE;
    }
    m_instaces++;
    m_socket = socket(af, type, protocol);
}

CSocket::~CSocket()
{
    m_instaces--;
    //Deinitialisation after last Socket dies
    if (m_instaces)
    {
        //Deinitialisations for Windows Sockets
        #ifdef _WIN32
        WSACleanup();
        //Deinitialisation for all other platforms
        #else

        #endif
        socketsInitialised = FALSE;
    }
    
}